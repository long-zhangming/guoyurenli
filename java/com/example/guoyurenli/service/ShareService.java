package com.example.guoyurenli.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;

public interface ShareService
{

	/**
	 * 分享接口，生成信息分享码
	 * @param model
	 * @param request
	 * @param response
	 */
	void share(Model model, HttpServletRequest request, HttpServletResponse response)throws Exception;

	/**
	 * 使用分享码
	 * @param model
	 * @param request
	 * @param code
	 * @return
	 */
	Object join(Model model, HttpServletRequest request, String code);

}
