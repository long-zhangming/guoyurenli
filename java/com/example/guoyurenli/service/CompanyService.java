package com.example.guoyurenli.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.example.guoyurenli.entity.GyFactory;

@Service
public interface CompanyService
{

	/**
	 * 添加招工企业REST
	 * @param factory
	 * @return
	 */
	Object addCompanydo(GyFactory factory);

	/**
	 * 公司列表
	 * @param request
	 * @param model
	 * @param pageNumber	第pageNumber页
	 * @param filter		模糊查询条件：%filter%
	 * @param attribute
	 * @return
	 */
	String companyList(HttpServletRequest request, Model model, Integer pageNumber, String filter, Integer attribute);

	/**
	 * 公司的详细信息
	 * @param model
	 * @param enterpriseId
	 * @return
	 */
	String postList(Model model, Integer enterpriseId) throws Exception;

}
