package com.example.guoyurenli.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.alibaba.fastjson.JSONObject;

public interface PostService
{

	/**
	 * 添加岗位MVC
	 * @param model
	 * @param company
	 * @param title
	 * @param address
	 * @return
	 */
	String postSave(Model model, Integer company, String title, String address);

	/**
	 * 添加岗位REST
	 * @param json
	 * @return
	 */
	Object postsavedo(JSONObject json);

	/**
	 * 岗位详细信息
	 * @param model
	 * @param postId
	 * @return
	 */
	String postfin(Model model, Integer postId);

	/**
	 * 岗位列表
	 * @param model
	 * @param request
	 * @param pageNumber
	 * @param filter
	 * @return
	 */
	String postList(Model model, HttpServletRequest request, Integer pageNumber, String filter);

	/**
	 * 更改岗位的“在招”“停招”状态
	 * @param jreq
	 * @return
	 */
	Object updateStatedo(JSONObject jreq);

}
