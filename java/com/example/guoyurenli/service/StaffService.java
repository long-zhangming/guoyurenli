package com.example.guoyurenli.service;

import org.springframework.ui.Model;

import com.alibaba.fastjson.JSONObject;
import com.example.guoyurenli.entity.Manpower;

public interface StaffService
{

	/**
	 * 增加人员Restful
	 * @param model		Model
	 * @param manpower	
	 * @return
	 */
	String staffSavedo(Model model, Manpower manpower);

	/**
	 * 删除一个人员的信息
	 * @param id	人员id
	 * @param operation	执行的操作，0删除/1雪藏
	 * @return
	 */
	Object staffDelece(Integer id, Integer operation);

	/**
	 * 人员列表数据
	 * @param page	
	 * @param limit	
	 * @param name
	 * @param operation
	 * @return
	 */
	Object stafflistdo(Integer page, Integer limit, String name, Integer operation);

	/**
	 * 修改求职人员的面试企业及时间等
	 * @param json
	 * @return
	 */
	Object updatePeople(JSONObject json)throws Exception;

	/**
	 * 状态改变
	 * @param json
	 * @return
	 */
	Object updateChange(JSONObject json);
	
}
