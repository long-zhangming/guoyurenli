package com.example.guoyurenli.service;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.example.guoyurenli.entity.GyUsers;

@Service
public interface UserService
{	
	/**
	 * 根据用户名查询用户信息,MyRealm中使用
	 * @param username	用户名
	 * @return	用户信息
	 */
	public GyUsers queryUserByUsername(String username);
	
	/**
	 * 根据用户名查询当前用户的角色列表---3张表连接查询,一个用户可以对应多个角色，所以用Set去重
	 * @param username	用户名
	 * @return	用户的角色名字（如，Boos、员工等）
	 */
	public Set<String> queryRoleNamesByUsername(String username);
	
	/**
	 * 根据用户名查询当前用户的权限---5张表连接查询
	 * @param username	用户名
	 * @return	用户的权限名字
	 */
	public Set<String> queryPermissionsByUsername(String username);
	
	/**
	 * 登录校验
	 * @param userName 	账号
	 * @param userPwd	密码
	 */
	public void checkLogin(String userName, String userPwd);
	
	/**
	 * 使用分享页面，根据id查询推荐人的信息
	 * @param id	推荐人ID
	 * @return		推荐人信息
	 */
	public GyUsers queryUserByUserid(int id);

	/**
	 * 用户修改密码
	 * @param userName	当前用户名
	 * @param userPwd	当前密码
	 * @param userPwdA	新密码1
	 * @param userPwdB	新密码2
	 * @return	处理结果
	 */
	public Object alterAskdo(String userName, String userPwd, String userPwdA, String userPwdB);

	/**
	 * Boos为别人注册新账号
	 * @param userName	用户名
	 * @param userPwd	密码
	 * @param userPwdA	确认密码
	 * @param roles		用户身份，员工、数据、供应商等等
	 * @return
	 */
	public Object registdo(String userName, String userPwd, String userPwdA, Integer roles);
		
}
