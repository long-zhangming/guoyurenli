package com.example.guoyurenli.service;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

public interface CompanyManagementService
{

	/**
	 * 公司管理，后台管理页面（数据）
	 * @param response HttpServletResponse
	 * @param page	第page页
	 * @param limit	每页limit条
	 * @param name	模糊查询：%name%
	 * @return
	 */
	Object factoryManagementdo(HttpServletResponse response
			, Integer page
			, Integer limit
			, String name) throws Exception;

	/**
	 * 企业的置顶、价格、时间、备注的修改
	 * @param json
	 * @return
	 */
	Object modifyCompanydo(JSONObject json);

	/**
	 * 删除一个公司
	 * @param json
	 * @return
	 */
	Object delectFactory(JSONObject json);

}
