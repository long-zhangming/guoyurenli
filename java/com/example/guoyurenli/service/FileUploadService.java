package com.example.guoyurenli.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FileUploadService
{

	/**
	 * 企业的招商简介文件上传
	 * @param request
	 * @return
	 */
	Object upload(HttpServletRequest request) throws Exception;

	/**
	 * 下载招聘简章
	 * @param request
	 * @param response
	 * @param realName
	 * @param guid
	 */
	void getCompanyFile(HttpServletRequest request
			, HttpServletResponse response
			, String realName, String guid) throws Exception;

	/**
	 * 厂区内容中插入的图片上传
	 * @param request
	 * @param response 
	 * @return	图片地址
	 */
	Object imgFileUpload(HttpServletRequest request);

}
