package com.example.guoyurenli.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.example.guoyurenli.entity.GyUsers;
import com.example.guoyurenli.mapper.UserMapper;
import com.example.guoyurenli.service.UserService;
import com.example.guoyurenli.util.MyUtil;

import af.spring.AfRestData;
import af.spring.AfRestError;

@Service
public class UserServiceImpl implements UserService
{
	@Resource
	private UserMapper userMapper;
	
	// MyRealm用,根据用户名查询用户信息，
	@Override
	public GyUsers queryUserByUsername(String username)
	{
		return userMapper.queryUserByUsername(username);
	}
	
	// MyRealm用,根据用户名查询当前用户的角色列表---3张表连接查询,一个用户可以对应多个角色，所以用Set去重
	@Override
	public Set<String> queryRoleNamesByUsername(String username)
	{
		return userMapper.queryRoleNamesByUsername(username);
	}
	
	// MyRealm用,根据用户名查询当前用户的权限---5张表连接查询
	@Override
	public Set<String> queryPermissionsByUsername(String username)
	{
		return userMapper.queryPermissionsByUsername(username);
	}
	
	// 登录校验
	@Override
	public void checkLogin(String userName, String userPwd)
	{
		// 得到Subject
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(userName, userPwd);
		subject.login(token);
	}

	// 使用分享页面，根据id查询推荐人的信息
	@Override
	public GyUsers queryUserByUserid(int id)
	{
		return userMapper.queryUserByUserid(id);
	}

	// 用户修改密码
	@Override
	public Object alterAskdo(String userName, String userPwd, String userPwdA, String userPwdB)
	{
		// 判断输入的两次新密码是否一致
		if(!userPwdA.equals(userPwdB)) {
			return new AfRestError("两次输入的密码不一致");
		}
		
		try
		{
			// 判断用户的原账号密码是否正确
			this.checkLogin(userName, userPwd);
		} catch (Exception e)
		{
			return new AfRestError("原用户名或密码输入有误!");
		}
		
		// 密码加密
		userPwdA = new Md5Hash(userPwdA).toString();
		
		// 修改密码
		GyUsers user = (GyUsers)SecurityUtils.getSubject().getPrincipal();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("password", userPwdA);
		map.put("id", user.user_id);
		userMapper.alterAsk(map);
		
		return new AfRestData("修改成功");
	}

	// 用户注册
	@Override
	public Object registdo(String userName, String userPassword, String userPasswordA, Integer roles)
	{
		// 判断密码逻辑
		if(MyUtil.isEmpty(userName) || MyUtil.isEmpty(userPassword)){
			return new AfRestError("账号或者密码输入有误。");
		}
		if(!userPassword.equals(userPasswordA)) {
			return new AfRestError("两次输入的密码不一致");
		}
		
		// 密码加密
		userPassword = new Md5Hash(userPassword).toString();
		
		GyUsers user = new GyUsers();
		user.username = userName;		// 账号
		user.password = userPassword;	// 密码
		user.timeCreate = new Date();	// 注册时间
		
		try
		{
			// 添加用户,返回ID
			userMapper.registGyUsers(user);
			// 为用户分配角色
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("uid", user.user_id);
			map.put("rid", roles);
			userMapper.registGyRoles(map);
		} catch (DuplicateKeyException e)
		{
			return new AfRestError("用户名重复");
		}
		
		return "regist";
	}

	

	

}
