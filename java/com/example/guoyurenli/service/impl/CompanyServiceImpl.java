package com.example.guoyurenli.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.example.guoyurenli.controller.PostController;
import com.example.guoyurenli.entity.GyFactory;
import com.example.guoyurenli.entity.GyUsers;
import com.example.guoyurenli.mapper.FactoryMapper;
import com.example.guoyurenli.service.CompanyService;
import com.example.guoyurenli.util.MyUtil;
import com.example.guoyurenli.util.TmpFile;

import af.spring.AfRestData;
import af.spring.AfRestError;

@Service
public class CompanyServiceImpl implements CompanyService
{
	@Resource
	FactoryMapper factoryMapper;
	
	// 添加招工企业REST
	@Override
	public Object addCompanydo(GyFactory factory)
	{
		// 取得当前用户
		GyUsers user = (GyUsers)SecurityUtils.getSubject().getPrincipal();
		
		// 字段处理
		factory.creator = user.user_id;		// 发布者
		factory.timecreate = new Date();	// 发布时间
		factory.suffix = MyUtil.getSuffix(factory.realName);		// 后缀名
		factory.storePath = TmpFile.getFactory().getAbsolutePath();	// 简章路径
		factory.timeupdata = new Date();	// 更新时间
		factory.topflag = false;	// 置顶
		factory.banflag = false;	// 加精
		factory.delflag = false;	// 删除
		factory.price = 0;			// 返费价格
		factory.expiration = 0;		// 返费时间
		
		// 招聘简章文件移动到永久文件夹
		if( !MyUtil.isEmpty(factory.realName))
		{
			// 名字不为空在给起guid
			factory.guid = MyUtil.guid2();		// guid:给文件起的新名字，避免文件名重复
			
			// 临时文件存放位置
			File tmpFile = TmpFile.getTmpFile(factory.realName);
			// 永久文件存放位置，guid + 后缀名
			File storeFile = TmpFile.getFactoryFile(factory.guid + factory.suffix);
			
			try{
				// 移动文件：临时文件夹到永久文件夹
				FileUtils.moveFile(tmpFile, storeFile);
			} catch (IOException e){
				return new AfRestError("文件移动失败！");
			}
		}
		
		// 富文本处理
		// 1、提取图片文件的文件名
		List<String> urlList = getImgName(factory.content);
		// 新imgurl存储列表
		List<String> newUrlList = new ArrayList<String>();
		// 2、通过文件名移动文件，把文件名列表替换成新路径
		for(String imgName : urlList)
		{
			// 2、通过文件名移动文件
			// 临时文件存储位置
			File tmpFile = TmpFile.getTmpFile(imgName);
			// 永久文件存储位置
			File storeFile = TmpFile.getImageUrlFile(imgName);
			try
			{
				// 移动文件
				FileUtils.moveFile(tmpFile, storeFile);
			} catch (IOException e)
			{
				System.out.println("富文本中图片文件移动失败" + tmpFile);
			}
			
			// 3、把图片的url替换成新地址
			newUrlList.add("/imageStore/" + imgName);
		}
		
		// 4、用新地址替换html√
		factory.content = replaceImgTag(factory.content, newUrlList);
		
		// 插入数据库
		factoryMapper.addFactory(factory);
		return new AfRestData("添加成功！");
	}

	// 公司列表
	@Override
	public String companyList(HttpServletRequest request
			, Model model
			, Integer pageNumber
			, String filter
			, Integer attribute)
	{
		// URL
		String url = request.getRequestURI();
		
		if(pageNumber == null)
			pageNumber = 1;
		
		// 模糊查询条件处理
		Map<String, Object> map = new HashMap<>();
		String like = null;
		if(!MyUtil.isEmpty(filter)){
			like = "%" + filter + "%";
		}
		map.put("like", like);
		
		// 未被雪藏
		if(attribute != null && attribute != 9 ){
			map.put("attribute", attribute);
		}
		
		// 一共多少条记录
		int count = factoryMapper.getCount(map);
		
		int pageSize = 10;		//每页显示多少条，自己设置
		int pageCount = count / pageSize;	//一共多少页
		if(count % pageSize != 0){ 	//如果不是刚好的倍数，就给页数+1
			pageCount += 1;
		}
		int startIndex = pageSize * (pageNumber - 1);	// 开始条数
		
		map.put("startIndex", startIndex);		// 开始条数
		map.put("pageSize", pageSize);			// 结束条数
		
		// 查询公司信息
		List<Map<String, Object>> factoryList = factoryMapper.factoryList(map);
		
		// 查询每个公司有几个岗位发布
		// 遍历每个Map,用map里面的id去岗位表里面查数量
		for(Map<String, Object> row : factoryList){
			int factoryId = (int) row.get("id");
			row.put("postcount", factoryMapper.getPostCount(factoryId));
		}
		
		model.addAttribute("factoryList", factoryList);
		model.addAttribute("pageCount", pageCount);
		model.addAttribute("pageNumber", pageNumber);
		model.addAttribute("url", url);
		
		return "enterprise/factorylist";
	}

	// 公司的详细信息
	@Override
	public String postList(Model model, Integer enterpriseId) throws Exception
	{
		if(enterpriseId == null)
			throw new Exception("出现错误，企业不存在，ID=null");
		
		// 根据ID查询特定公司的信息
		GyFactory factory = factoryMapper.getFactory(enterpriseId);
		
		// 根据公司查询公司下所有的招聘信息
		List<Map<String, Object>> postlist = factoryMapper.getPostList(factory.id);
		postlist = PostController.changge(postlist);
		
		// 放入Model
		model.addAttribute("postsize", postlist.size());
		model.addAttribute("factory", factory);
		model.addAttribute("postlist", postlist);	// 岗位信息
		
		return "enterprise/factoryfin";
	}
	
	/**
	 * 富文本中图片名字提取
	 * @param	 	htmlStr <img src="/imageTmp/16061168383191.png" alt="" />
	 * @return		16061168383191.png
	 */
	private List<String> getImgName(String htmlStr)
	{
		List<String> list = new ArrayList<>();
		String regex = "<img.*?src\\s*=\\s*['\"]{1}/imageTmp/(.*?)['\"]{1}[^>]*?>";
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(htmlStr);
		while (m.find()){
			list.add(m.group(1));
		}
		return list;
	}
	
	/**
	 * 富文本中url地址的替换，临时路径替换成永久路径
	 * @param htmlStr	替换后的html
	 * @param url		url名字列表
	 * @return
	 */
	private String replaceImgTag(String htmlStr, List<String> strList)
	{
		String pattern="<img.*?src\\s*=\\s*['\"]{1}(/imageTmp/.*?)['\"]{1}[^>]*?>";
        StringBuffer operatorStr = new StringBuffer(htmlStr);
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(htmlStr);
        int i = 0;
        while(m.find()) {
            operatorStr.replace(m.start(1), m.end(1), strList.get(i));
            m = p.matcher(operatorStr);
            i ++;
            if(i == strList.size())
            	i = 0;
        }
        return operatorStr.toString();
	}
}
