package com.example.guoyurenli.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.guoyurenli.service.FileUploadService;
import com.example.guoyurenli.util.MyUtil;
import com.example.guoyurenli.util.TmpFile;

import af.spring.AfRestData;
import af.spring.AfRestError;

@Service
public class FileUploadServiceImpl implements FileUploadService
{
	// 企业的招商简介文件上传
	@Override
	public Object upload(HttpServletRequest request) throws Exception
	{
		MultipartHttpServletRequest mhr = (MultipartHttpServletRequest) request;
		
		// 附带的字段
//		String tag = mhr.getParameter("tag");
		
		MultipartFile mf = mhr.getFile("file");		// 表单的name='file'
		Map<String, Object> result = new HashMap<>();
		if(mf != null && !mf.isEmpty())
		{	
			// 取得文件的名称并保存
			String realName = mf.getOriginalFilename();		// 取得文件的名字，realName:数据库.txt
			if(!MyUtil.isFileRules(realName)) {
				return new AfRestError("此类型的文件不允许上传!");
			}
			
			File tmpFile = TmpFile.getTmpFile(realName);		// file:c:/guoyufile/tmp/数据库.txt
			
			// 接收上传并保存到临时文件
			mf.transferTo(tmpFile);
			
			// 回应给客户端的消息
			result.put("realNmae", realName);	// 原名称
		}
		
		return new AfRestData(result);
	}
	
	// 厂区内容中插入的图片上传
	@Override
	public Object imgFileUpload(HttpServletRequest request)
	{
		MultipartHttpServletRequest mhr = (MultipartHttpServletRequest) request;
		// 返回值
		JSONObject json = new JSONObject();
		
		// 前端的那个东西固定写法，后面还带一个dir参数，URL中的，代表上传的文件类型，
		// imgFile: 文件form名称
		MultipartFile mf = mhr.getFile("imgFile");
		if(mf != null && !mf.isEmpty())
		{
			// 给文件起个新名字
			String realName = mf.getOriginalFilename();		// 取得文件的名字，realName:xxx.txt
			String guid = MyUtil.guid2();
			String storeFile = guid + MyUtil.getSuffix(realName);	// 文件新名字，guid+后缀名；
			
			// 零时文件目录 + 文件名字 = 一个文件的路径
			File tmpFile = TmpFile.getTmpFile(storeFile);
			
			try
			{
				// 接收上传并保存到临时文件，成功的返回字段
				mf.transferTo(tmpFile);
				
				String url = "/imageTmp/" + storeFile;
				json.put("error", 0);	// 表示无错误
				json.put("url", url);	// 此字段就是能访问到图片的url
				
//				{
//			        "error" : 0,
//			        "url" : "/image？349437843957.jpg"
//				}
			} catch (Exception e)
			{
				// 上传失败返回的字段
				json.put("error", 1);
				json.put("message", e.getMessage());
//				{
//			        "error" : 1,
//			        "message" : "错误信息"
//				}
			}
		}
		return json;
	}

	// 下载招聘简章
	@Override
	public void getCompanyFile(HttpServletRequest request
			, HttpServletResponse response
			, String realName
			, String guid) throws Exception
	{
		// 判断文件是否存在
		if(MyUtil.isEmpty(realName)){
			response.sendError(404, "您要下载的文件不存在");
			return;
		}
		
		// 总存储文件夹
		File dataDir = TmpFile.getFactory();
		String suffix = MyUtil.getSuffix(realName);	// 后缀
		
		// 文件名
		String fileName = guid + suffix;
		
		// 需要的总文件路径
		File targetFile = new File(dataDir, fileName);
		
		// 检查目标文件是否存在
		if (!targetFile.exists() || !targetFile.isFile())
		{
			response.sendError(404, "您要下载的文件不存在");
			return;
		}
		
		//设置应答：Content-Type(文件类型) / Content-Length(文件大小) ，读取目标文件，发送到客户端
		String contentType = MyUtil.getContentType(suffix);	// .ppt (文件后缀名)
		long contentLength = targetFile.length();
		response.setContentType(contentType);  			// 告诉浏览器文件是什么类型
		response.setHeader("Content-Length", String.valueOf(contentLength)); 	// 告诉浏览器文件的长度
		response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(realName, "UTF-8"));	// 告诉浏览器下载的文件名,再提示一个下载框：13.png
																			// 后面传入文件名（提示前端的文件名，可以自己定义）
		
		// 应答：读取目标文件数据，发送给客户端
		InputStream inputStream = new FileInputStream(targetFile);	// 读取文件
		OutputStream outputStream = response.getOutputStream();		// 发送给浏览器
		
		try
		{
			MyUtil.copy(inputStream, outputStream);
		} catch (Exception e)
		{
			try{ inputStream.close(); } catch (Exception e2){}
		}
		
		outputStream.close();
		
	}
}
