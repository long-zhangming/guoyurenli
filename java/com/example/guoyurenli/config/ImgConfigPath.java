package com.example.guoyurenli.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// 厂区内容中图片的插入后回显图片的路径映射
@Component
public class ImgConfigPath implements WebMvcConfigurer
{
	/**
	 * 图片回显虚拟路径映射配置
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry)
	{
		// 临时文件夹中图片的回显映射
		registry.addResourceHandler("/imageTmp/**").addResourceLocations("file:/c:/guoyufile/tmp/");
		// 正式文件中图片的显示映射
		registry.addResourceHandler("/imageStore/**").addResourceLocations("file:/c:/guoyufile/factoryImg/");
		
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}
	
}
