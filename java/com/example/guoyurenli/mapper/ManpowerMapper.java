package com.example.guoyurenli.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import com.example.guoyurenli.entity.Manpower;

@Mapper
public interface ManpowerMapper
{
	/**
	 * 插入一个人员信息
	 * @param manpower
	 */
	public void addManpower(Manpower manpower);
	
	/**
	 * 查询人员的个数
	 * @param map
	 * @return
	 */
	public int getManpower(Map<String, Object> map);
	
	/**
	 * 查询所有人员信息
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getManpowerList(Map<String, Object> map);
	
	/**
	 * 删除一个人
	 * @param id
	 */
	@Delete("DELETE FROM `manpower` WHERE id=#{id}")
	public void deleteManpower(Integer id);
	
	/**
	 * 雪藏一个人
	 * @param id
	 */
	@Update("UPDATE `gy_ums` SET state=7 WHERE manpowerid=#{id}")
	public void concealManpower(Integer id);
	
	/**
	 * 修改一个人的状态
	 * @param map
	 */
	public void conditionManpower(Map<String, Object> map);

	/**
	 * 修改人员信息
	 * @param map
	 */
	@Update("UPDATE `manpower` SET ${key} WHERE id = #{id}")
	public void updateManpower(Map<String, Object> map);

}
