package com.example.guoyurenli.mapper;

import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.guoyurenli.entity.GyUsers;

@Mapper
public interface UserMapper
{
	/**
	 * 根据用户名查询用户信息
	 * @param username
	 * @return
	 */
	public GyUsers queryUserByUsername(String username);

	/**
	 * 根据用户名查询当前用户的角色列表---3张表连接查询,一个用户可以对应多个角色，所以用Set去重
	 * @param username
	 * @return
	 */
	public Set<String> queryRoleNamesByUsername(String username);

	/**
	 * 根据用户名查询当前用户的权限---5张表连接查询
	 * @param username
	 * @return
	 */
	public Set<String> queryPermissionsByUsername(String username);
	
	/**
	 * 使用分享页面，根据id查询推荐人的信息
	 * @param id
	 * @return
	 */
	@Select("SELECT user_id, username, `password` FROM `gy_users` WHERE user_id=#{user_id}")
	public GyUsers queryUserByUserid(int id);
	
	/**
	 * 注册用户信息
	 * @param user
	 */
	public void registGyUsers(GyUsers user);
	
	/**
	 * 为注册的用户分配角色
	 * @param map
	 */
	public void registGyRoles(Map<String, Object> map);

	/**
	 * 修改密码
	 * @param map
	 */
	@Update("UPDATE `gy_users` SET `password`= #{password} WHERE user_id=#{id}")
	public void alterAsk(Map<String, Object> map);
}
