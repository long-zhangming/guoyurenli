package com.example.guoyurenli.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.guoyurenli.entity.GyLabel;
import com.example.guoyurenli.entity.GyPls;
import com.example.guoyurenli.entity.GyPost;

@Mapper
public interface PostMapper
{
	/**
	 * 查询所有的标签（福利）,添加岗位的时候显示用
	 * @return
	 */
	@Select("SELECT `id`, `title` FROM `gy_label`")
	public List<GyLabel> getLabel();

	/**
	 * 添加一个岗位
	 * @param post
	 */
	public void addPost(GyPost post);

	/**
	 * 添加一个岗位的标签（优势、福利）
	 * @param pls
	 */
	@Insert("INSERT INTO `gy_pls` (`pid`, `lid`) VALUES(#{pid}, #{lid})")
	public void addLabel(GyPls pls);

	/**
	 * 根据ID查询岗位的信息
	 * @param postId
	 * @return
	 */
	@Select("SELECT * FROM `gy_post` WHERE id=#{id}")
	public List<Map<String, Object>> getPost(Integer postId);

	/**
	 * 通过岗位ID查询岗位对应的标签
	 * @param pid
	 * @return
	 */
	public Set<String> getPostLabel(Integer pid);
	
	/**
	 * 查询岗位信息的数量
	 * @param map
	 * @return
	 */
	public Integer getPostConut(Map<String, Object> map);

	/**
	 * 查询所有岗位信息
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> getPostList(Map<String, Object> map);

	/**
	 * 修改一个岗位的“在招”“停招”状态
	 * @param map
	 */
	@Update("UPDATE `gy_post` SET state=#{state}, timeupdate=#{timeupdate} WHERE id=#{id}")
	public void updateState(Map<String, Object> map);

	/**
	 * 修改一个公司下所有岗位的的“在招”“停招”状态
	 * @param mapa
	 */
	@Update("UPDATE `gy_post` SET state=#{state}, timeupdate=#{timeupdate} WHERE company=#{company}")
	public void updateStates(Map<String, Object> mapa);
	
	
}
