package com.example.guoyurenli.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.codec.binary.Hex;

public class MyUtil
{
	static AtomicInteger seed = new AtomicInteger(0);

	/**
	 * 把字符串转换成MD5值返回
	 * @param text  字符串
	 * @return		md5值
	 */
	public static String md5(String text)
	{
		try
		{
			// 转成字节数据
			byte[] data = text.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data);
			byte[] code = md.digest();

			// 转成HEX显示
			return Hex.encodeHexString(code, true);
		} catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * @return	返回不重复的字母字符串
	 */
	public static String guid()
	{
		String s = UUID.randomUUID().toString();
		String s2 = s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23)
				+ s.substring(24);
		return s2.toUpperCase();
	}

	/**
	 * 
	 * @return	返回不重复的数字字符串
	 */
	public static String guid2()
	{
		int s = seed.incrementAndGet() % 10;
		return System.currentTimeMillis() + "" + s;
	}

	/**
	 * 判断字符串是不是空串或为Null,为空返回true
	 * @param s		字符串
	 * @return		boolean
	 */
	public static boolean isEmpty(String s)
	{
		return s == null || s.length() == 0;
	}

	/**
	 * 查找文件的后缀名
	 * @param filePath	文件名，如xxxx.jpg
	 * @return	后缀名，如.jpg
	 */
	public static String getSuffix(String filePath)
	{
		int p1 = filePath.lastIndexOf('.');
		if (p1 > 0)
		{
			String suffix = filePath.substring(p1);
			if (suffix.length() < 10) // 后缀长度必须小于10
			{
				// 后缀中不能有路径分隔符
				if (suffix.indexOf('/') < 0 && suffix.indexOf('\\') < 0)
					return suffix.toLowerCase();
			}
		}
		return "";
	}

	/**
	 * 根据后缀名，推算 Content-Type（文件类型）
	 * @param suffix	后缀名，如.jpg
	 * @return		Content-Type,如image/jpg
	 */
	public static String getContentType(String suffix)
	{
		// 常用文件
		suffix = suffix.toLowerCase();
		if (suffix.equals(".jpg"))
			return "image/jpg";
		if (suffix.equals(".jpeg"))
			return "image/jpeg";
		if (suffix.equals(".png"))
			return "image/png";
		if (suffix.equals(".gif"))
			return "image/gif";
		if (suffix.equals(".html"))
			return "text/html";
		if (suffix.equals(".txt"))
			return "text/plain";
		if (suffix.equals(".js"))
			return "application/javascript";
		if (suffix.equals(".mp4"))
			return "video/mp4";
		
		// PPT等文件
		if (suffix.equals(".txt"))
			return "text/plain";
		if (suffix.equals(".xls"))
			return "application/x-xls";
		if (suffix.equals(".doc"))
			return "application/msword";
		if (suffix.equals(".ppt"))
			return "application/x-ppt";
		if (suffix.equals(".png"))
			return "application/x-png";
		if (suffix.equals(".jpg"))
			return "application/x-jpg";
		if (suffix.equals(".pdf"))
			return "application/pdf";
		
		return "application/octet-stream"; // 一般的二进制文件类型
	}
	
	/**
	 * 判断文件的格式是否符合规则，我么只允许必要的文件上传，对于那些不能识别的文件，不允许上传
	 * @param fileName	文件名字，会自动计算文件后缀名，通过后缀名判断文件是否符合
	 * @return
	 */
	public static Boolean isFileRules(String fileName)
	{
		// 文件后缀名
		String suffix = getSuffix(fileName);
		
		// 常用文件
		if (suffix.equals(".jpg"))
			return true;
		if (suffix.equals(".jpeg"))
			return true;
		if (suffix.equals(".png"))
			return true;
		if (suffix.equals(".gif"))
			return true;
		if (suffix.equals(".html"))
			return true;
		if (suffix.equals(".txt"))
			return true;
		
		// PPT等文件
		if (suffix.equals(".xls"))
			return true;
		if (suffix.equals(".doc"))
			return true;
		if (suffix.equals(".ppt"))
			return true;
		if (suffix.equals(".pdf"))
			return true;
		
		return false;
	}

	/**
	 * 拷贝字节流，从in中读取字节，写入到out中
	 * @param in
	 * @param out
	 * @return
	 * @throws Exception
	 */
	public static long copy(InputStream in, OutputStream out) throws Exception
	{
		long count = 0;
		byte[] buf = new byte[8192];
		while (true)
		{
			int n = in.read(buf);
			if (n < 0)
				break;
			if (n == 0)
				continue;
			out.write(buf, 0, n);
			count += n;
		}
		return count;
	}
}
