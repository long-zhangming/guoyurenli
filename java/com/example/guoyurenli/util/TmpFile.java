package com.example.guoyurenli.util;

import java.io.File;


/** 
 * 临时文件的位置、永久文件位置
 *
 */
public class TmpFile
{
	/**
	 * 获取临时文件根目录
	 * @return	临时文件根目录：c:/guoyufile/tmp
	 */
	public static File getDir()
	{
		return new File("c:/guoyufile/tmp");
	}
	
	/**
	 * 获取临时文件总路径：根目录+文件名
	 * @param tmpName	文件名
	 * @return	临时文件总目录：c:/guoyufile/tmp/xxxx.ppt
	 */
	public static File getTmpFile(String tmpName)
	{
		File dir = getDir();
		dir.mkdirs();
		return new File(dir, tmpName);
	}
	
	/**
	 * 招聘简章等文件根目录
	 * @return	招聘简章等文件根目录:c:/guoyufile/factory
	 */
	public static File getFactory()
	{
		return new File("c:/guoyufile/factory");
	}
	
	/**
	 * 招聘简章等文件总目录，招聘简章根目录+招聘简章文件名
	 * @param tmpName	招聘简章文件名
	 * @return	招聘简章文件总目录：c:/guoyufile/factory/xxxxx.ppt
	 */
	public static File getFactoryFile(String tmpName)
	{
		File dir = getFactory();
		dir.mkdirs();
		return new File(dir, tmpName);
	}
	
	/**
	 * 工厂简介中富文本中图片的存储根目录
	 * @return  富文本图片存储的根目录：C:/guoyufile/factoryImg
	 */
	public static File getImageUrl()
	{
		return new File("c:/guoyufile/factoryImg");
	}
	
	/**
	 * 工厂简介中富文本中图片的存储总目录，根目录+文件名
	 * @param imageName		图片文件的文件名
	 * @return	根目录+文件名c:/guoyufile/factoryImg/xxxxx.jpg
	 */
	public static File getImageUrlFile(String imageName)
	{
		File dir = getImageUrl();
		dir.mkdirs();
		return new File(dir, imageName);
	}
	
	
}
