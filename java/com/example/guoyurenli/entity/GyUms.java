package com.example.guoyurenli.entity; 

import java.util.Date; 

/** 本类由 POJO生成器 自动生成于 2020-07-14 16:41:33
    作者：阿发你好      官网: http://afanihao.cn 
*/ 

/** INSERT语句 ( 预处理方式 ) 
  INSERT INTO `gy_ums`
        (`manpowerid`, `state`, `entryunit`, `dateofentry`, `rebates`, `rebatestime`, `reasonforfailure`) 
  VALUES(?, ?, ?, ?, ?, ?, ?) 
*/ 

/** INSERT语句 ( MyBatis方式 ) 
  INSERT INTO `gy_ums`
        (`manpowerid`, `state`, `entryunit`, `dateofentry`, `rebates`, `rebatestime`, `reasonforfailure`) 
  VALUES(#{manpowerid}, #{state}, #{entryunit}, #{dateofentry}, #{rebates}, #{rebatestime}, #{reasonforfailure}) 

  自增主键: 无
*/ 

// 求职者状态类
public class GyUms 
{ 
 
	public Integer manpowerid ; 	// 求职者ID，对应ManpowerID
	public Byte state ; 			// 求职者状态:（填写资料0、确认1、报名成功2、报名失败3、面试成功4、面试失败5、入职成功6、被雪藏7、离职8）
	public String entryunit ; 		// 入职公司
	public Date dateofentry ; 		// 入职时间
	public Double rebates ; 		// 返费金额
	public Date rebatestime ; 		// 发放时间
	public String reasonforfailure ; 	// 失败原因:(对应报名失败、面试失败、离职的原因)


	public void setManpowerid(Integer manpowerid)
	{
		this.manpowerid=manpowerid;
	}
	public Integer getManpowerid()
	{
		return this.manpowerid;
	}
	public void setState(Byte state)
	{
		this.state=state;
	}
	public Byte getState()
	{
		return this.state;
	}
	public void setEntryunit(String entryunit)
	{
		this.entryunit=entryunit;
	}
	public String getEntryunit()
	{
		return this.entryunit;
	}
	public void setDateofentry(Date dateofentry)
	{
		this.dateofentry=dateofentry;
	}
	public Date getDateofentry()
	{
		return this.dateofentry;
	}
	public void setRebates(Double rebates)
	{
		this.rebates=rebates;
	}
	public Double getRebates()
	{
		return this.rebates;
	}
	public void setRebatestime(Date rebatestime)
	{
		this.rebatestime=rebatestime;
	}
	public Date getRebatestime()
	{
		return this.rebatestime;
	}
	public void setReasonforfailure(String reasonforfailure)
	{
		this.reasonforfailure=reasonforfailure;
	}
	public String getReasonforfailure()
	{
		return this.reasonforfailure;
	}

} 
 