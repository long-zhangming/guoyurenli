package com.example.guoyurenli.entity; 

import java.util.Date; 

/** 本类由 POJO生成器 自动生成于 2020-08-06 14:13:59
    作者：阿发你好      官网: http://afanihao.cn 
*/ 

/** INSERT语句 ( 预处理方式 ) 
  INSERT INTO `gy_post`
        (`id`, `company`, `creator`, `title`, `content`, `workaddress`, `experience`, `academicDegree`, `wagesmin`, `wagesmax`, `timerelease`, `timeupdate`, `hiring`, `state`, `topflag`, `banflag`, `delflag`) 
  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
*/ 

/** INSERT语句 ( MyBatis方式 ) 
  INSERT INTO `gy_post`
        (`id`, `company`, `creator`, `title`, `content`, `workaddress`, `experience`, `academicDegree`, `wagesmin`, `wagesmax`, `timerelease`, `timeupdate`, `hiring`, `state`, `topflag`, `banflag`, `delflag`) 
  VALUES(#{id}, #{company}, #{creator}, #{title}, #{content}, #{workaddress}, #{experience}, #{academicDegree}, #{wagesmin}, #{wagesmax}, #{timerelease}, #{timeupdate}, #{hiring}, #{state}, #{topflag}, #{banflag}, #{delflag}) 

  自增主键: id
*/ 

// 职位类
public class GyPost 
{ 
 
	public Integer id ; 		// ID 
	public Integer company ; 	// 所属公司ID
	public Integer creator ; 	// 发布者ID
	public String title ; 		// 职位名称
	public String content ; 	// 职位详情介绍
	public String workaddress ; // 工作地点
	public Byte experience ; 	// 经验要求/年:（不限0、1年减1、1-3年2、3-5年3、5、10年4、10年上5）
	public Byte academicDegree ; // 学历要求:（不限0、高中1、职高2、中专3、大专4、本科5、硕士6、博士7）
	public Integer wagesmin ; 	// 最低工资
	public Integer wagesmax ; 	// 最高工资
	public Date timerelease ; 	// 发布时间	
	public Date timeupdate ; 	// 更新时间
	public Integer hiring ; 	// 招聘人数
	public Byte state ; 		// 在招/停招
	public Byte topflag ; 		// 置顶标识
	public Byte banflag ; 		// 加精标识
	public Byte delflag ; 		// 删除标识


	public void setId(Integer id)
	{
		this.id=id;
	}
	public Integer getId()
	{
		return this.id;
	}
	public void setCompany(Integer company)
	{
		this.company=company;
	}
	public Integer getCompany()
	{
		return this.company;
	}
	public void setCreator(Integer creator)
	{
		this.creator=creator;
	}
	public Integer getCreator()
	{
		return this.creator;
	}
	public void setTitle(String title)
	{
		this.title=title;
	}
	public String getTitle()
	{
		return this.title;
	}
	public void setContent(String content)
	{
		this.content=content;
	}
	public String getContent()
	{
		return this.content;
	}
	public void setWorkaddress(String workaddress)
	{
		this.workaddress=workaddress;
	}
	public String getWorkaddress()
	{
		return this.workaddress;
	}
	public void setExperience(Byte experience)
	{
		this.experience=experience;
	}
	public Byte getExperience()
	{
		return this.experience;
	}
	public void setAcademicDegree(Byte academicDegree)
	{
		this.academicDegree=academicDegree;
	}
	public Byte getAcademicDegree()
	{
		return this.academicDegree;
	}
	public void setWagesmin(Integer wagesmin)
	{
		this.wagesmin=wagesmin;
	}
	public Integer getWagesmin()
	{
		return this.wagesmin;
	}
	public void setWagesmax(Integer wagesmax)
	{
		this.wagesmax=wagesmax;
	}
	public Integer getWagesmax()
	{
		return this.wagesmax;
	}
	public void setTimerelease(Date timerelease)
	{
		this.timerelease=timerelease;
	}
	public Date getTimerelease()
	{
		return this.timerelease;
	}
	public void setTimeupdate(Date timeupdate)
	{
		this.timeupdate=timeupdate;
	}
	public Date getTimeupdate()
	{
		return this.timeupdate;
	}
	public void setHiring(Integer hiring)
	{
		this.hiring=hiring;
	}
	public Integer getHiring()
	{
		return this.hiring;
	}
	public void setState(Byte state)
	{
		this.state=state;
	}
	public Byte getState()
	{
		return this.state;
	}
	public void setTopflag(Byte topflag)
	{
		this.topflag=topflag;
	}
	public Byte getTopflag()
	{
		return this.topflag;
	}
	public void setBanflag(Byte banflag)
	{
		this.banflag=banflag;
	}
	public Byte getBanflag()
	{
		return this.banflag;
	}
	public void setDelflag(Byte delflag)
	{
		this.delflag=delflag;
	}
	public Byte getDelflag()
	{
		return this.delflag;
	}

} 
 