package com.example.guoyurenli.entity; 

import java.util.Date; 

/** 本类由 POJO生成器 自动生成于 2020-08-14 15:43:48
    作者：阿发你好      官网: http://afanihao.cn 
*/ 

/** INSERT语句 ( 预处理方式 ) 
  INSERT INTO `gy_factory`
        (`id`, `creator`, `title`, `nature`, `address`, `content`, `attribute`, `timecreate`, `storePath`, `realName`, `guid`, `suffix`, `topflag`, `banflag`, `delflag`, `price`, `expiration`, `remarks`, `timeupdata`) 
  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
*/ 

/** INSERT语句 ( MyBatis方式 ) 
  INSERT INTO `gy_factory`
        (`id`, `creator`, `title`, `nature`, `address`, `content`, `attribute`, `timecreate`, `storePath`, `realName`, `guid`, `suffix`, `topflag`, `banflag`, `delflag`, `price`, `expiration`, `remarks`, `timeupdata`) 
  VALUES(#{id}, #{creator}, #{title}, #{nature}, #{address}, #{content}, #{attribute}, #{timecreate}, #{storePath}, #{realName}, #{guid}, #{suffix}, #{topflag}, #{banflag}, #{delflag}, #{price}, #{expiration}, #{remarks}, #{timeupdata}) 

  自增主键: id
*/ 

// 工厂企业类
public class GyFactory 
{ 
	public Integer id ; 		// id
	public Integer creator ; 	// 发布者
	public String title ; 		// 公司名称
	public Integer nature ; 	// 公司性质:（互联网0、制造业1、人力资源2、服务业3、人工智能4、待定XXX5）
	public String address ; 	// 公司地址
	public String content ; 	// 公司简介
	public Byte attribute ; 	// 公司性质:（非信产0、渝北信产1、北碚信产2、市信产3、市外企业4）
	public Date timecreate ; 	// 创建时间
	public String storePath ; 	// 简章存放路径
	public String realName ; 	// 简章原名
	public String guid ; 		// 简章新名(生成的不重复名字)
	public String suffix ; 		// 简章文件后缀名
	public Boolean topflag ; 	// 置顶标识
	public Boolean banflag ; 	// 岗位可操作标识
	public Boolean delflag ; 	// 删除标识（用定时任务删除）
	public Integer price ; 		// 返费价格
	public Integer expiration ; // 返费周期（多少天天）
	public String remarks ; 	// 返费详情备注
	public Date timeupdata ; 	// 最近更新


	public void setId(Integer id)
	{
		this.id=id;
	}
	public Integer getId()
	{
		return this.id;
	}
	public void setCreator(Integer creator)
	{
		this.creator=creator;
	}
	public Integer getCreator()
	{
		return this.creator;
	}
	public void setTitle(String title)
	{
		this.title=title;
	}
	public String getTitle()
	{
		return this.title;
	}
	public void setNature(Integer nature)
	{
		this.nature=nature;
	}
	public Integer getNature()
	{
		return this.nature;
	}
	public void setAddress(String address)
	{
		this.address=address;
	}
	public String getAddress()
	{
		return this.address;
	}
	public void setContent(String content)
	{
		this.content=content;
	}
	public String getContent()
	{
		return this.content;
	}
	public void setAttribute(Byte attribute)
	{
		this.attribute=attribute;
	}
	public Byte getAttribute()
	{
		return this.attribute;
	}
	public void setTimecreate(Date timecreate)
	{
		this.timecreate=timecreate;
	}
	public Date getTimecreate()
	{
		return this.timecreate;
	}
	public void setStorePath(String storePath)
	{
		this.storePath=storePath;
	}
	public String getStorePath()
	{
		return this.storePath;
	}
	public void setRealName(String realName)
	{
		this.realName=realName;
	}
	public String getRealName()
	{
		return this.realName;
	}
	public void setGuid(String guid)
	{
		this.guid=guid;
	}
	public String getGuid()
	{
		return this.guid;
	}
	public void setSuffix(String suffix)
	{
		this.suffix=suffix;
	}
	public String getSuffix()
	{
		return this.suffix;
	}
	public void setTopflag(Boolean topflag)
	{
		this.topflag=topflag;
	}
	public Boolean getTopflag()
	{
		return this.topflag;
	}
	public void setBanflag(Boolean banflag)
	{
		this.banflag=banflag;
	}
	public Boolean getBanflag()
	{
		return this.banflag;
	}
	public void setDelflag(Boolean delflag)
	{
		this.delflag=delflag;
	}
	public Boolean getDelflag()
	{
		return this.delflag;
	}
	public void setPrice(Integer price)
	{
		this.price=price;
	}
	public Integer getPrice()
	{
		return this.price;
	}
	public void setExpiration(Integer expiration)
	{
		this.expiration=expiration;
	}
	public Integer getExpiration()
	{
		return this.expiration;
	}
	public void setRemarks(String remarks)
	{
		this.remarks=remarks;
	}
	public String getRemarks()
	{
		return this.remarks;
	}
	public void setTimeupdata(Date timeupdata)
	{
		this.timeupdata=timeupdata;
	}
	public Date getTimeupdata()
	{
		return this.timeupdata;
	}
	
	@Override
	public String toString()
	{
		return "GyFactory [id=" + id + ", creator=" + creator + ", title=" + title + ", nature=" + nature + ", address="
				+ address + ", content=" + content + ", attribute=" + attribute + ", timecreate=" + timecreate
				+ ", storePath=" + storePath + ", realName=" + realName + ", guid=" + guid + ", suffix=" + suffix
				+ ", topflag=" + topflag + ", banflag=" + banflag + ", delflag=" + delflag + ", price=" + price
				+ ", expiration=" + expiration + ", remarks=" + remarks + ", timeupdata=" + timeupdata + "]";
	}
	

} 
 