package com.example.guoyurenli.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.alibaba.fastjson.JSONObject;
import com.example.guoyurenli.service.PostService;

// 管理岗位的
@Controller
public class PostController
{
	@Autowired
	PostService postService;
	
	// 添加岗位MVC
	@GetMapping("postsave") 
	public String postSave(Model model, Integer company, String title, String address)
	{
		return postService.postSave(model, company, title, address);
	}
	
	// 添加岗位REST
	@PostMapping("postsave.do")
	public Object postsavedo(@RequestBody JSONObject json)
	{
		return postService.postsavedo(json);
	}
	
	// 岗位详细信息
	@GetMapping("/u/postfin")
	public String postfin(Model model, Integer postId)
	{
		return postService.postfin(model, postId);
	}
	
	// 岗位列表
	@GetMapping("/u/post/list")
	public String postList(Model model
			, HttpServletRequest request
			, Integer pageNumber
			, String filter)
	{
		return postService.postList(model, request, pageNumber, filter);
	}
	
	// 更改岗位的“在招”“停招”状态
	@PostMapping("/enterprise/updateState.do")
	public Object updateStatedo(@RequestBody JSONObject jreq) throws Exception
	{
		return postService.updateStatedo(jreq);
	}
	
	
	// 处理岗位字段的特殊类型
	public static List<Map<String, Object>> changge(List<Map<String, Object>> postlist)
	{
		// 处理岗位的条件字段
		for(Map<String, Object> row : postlist)
		{
			// 工作经验字段
			int experience = (int)row.get("experience");
			switch (experience)
			{
				case 0:
					row.put("experience", "经验不限");
					break;
				case 1:
					row.put("experience", "1年以下");
					break;
				case 2:
					row.put("experience", "1-3年");
					break;
				case 3:
					row.put("experience", "3-5年");
					break;
				case 4:
					row.put("experience", "5-10年");
					break;
				case 5:
					row.put("experience", "10年以上");
					break;
				default:
					break;
			}
			
			// 学历要求字段
			int academicDegree = (int)row.get("academicDegree");
			switch (academicDegree)
			{
				case 0:
					row.put("academicDegree", "学历不限");
					break;
				case 1:
					row.put("academicDegree", "高中");
					break;
				case 2:
					row.put("academicDegree", "技校");
					break;
				case 3:
					row.put("academicDegree", "中专");
					break;
				case 4:
					row.put("academicDegree", "大专");
					break;
				case 5:
					row.put("academicDegree", "本科");
					break;
				case 6:
					row.put("academicDegree", "硕士");
					break;
				case 7:
					row.put("academicDegree", "博士");
					break;
				default:
					break;
			}
			
//			// 状态字段
//			int state = (int)row.get("state");
//			switch (state)
//			{
//				case 0:
//					row.put("state", "在招");
//					break;
//				case 1:
//					row.put("state", "停招");
//					break;
//				default:
//					break;
//			}
		}
		
		return postlist;
	}

	
}
