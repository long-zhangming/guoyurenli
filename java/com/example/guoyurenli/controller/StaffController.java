package com.example.guoyurenli.controller;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.alibaba.fastjson.JSONObject;
import com.example.guoyurenli.entity.GyUsers;
import com.example.guoyurenli.entity.Manpower;
import com.example.guoyurenli.service.StaffService;

@Controller
public class StaffController
{	
	@Resource
	StaffService staffService;
	
	// 增加人员页面
	@GetMapping("staffSave")
	public String staffSave(Model model)
	{
		// 取得当前用户的信息放入会话
		GyUsers user = (GyUsers)SecurityUtils.getSubject().getPrincipal();
		model.addAttribute("user", user);
		return "staff/save";
	}
	
	// 增加人员Restful
	@PostMapping("staffSave.do")
	public String staffSavedo(Model model, Manpower manpower)
	{
		return staffService.staffSavedo(model, manpower);
	}
	
	// 删除一个人员的信息
	@PostMapping("staffDelece.do")
	public Object staffDelece(Integer id, Integer operation)
	{
		return staffService.staffDelece(id, operation);
	}
	
	// 人员列表页面
	@GetMapping("stafflist")
	public String stafflist()
	{
		return "staff/list";
	}
	
	// 全部人员列表页面
	@GetMapping("staffAlllist")
	public String staffAlllist()
	{
		return "staff/listFB";
	}
	
	// 人员列表数据
	@GetMapping("stafflist.do")
	public Object stafflistdo(Integer page, Integer limit, String name, Integer operation)
	{
		return staffService.stafflistdo(page, limit, name, operation);
	}
	
	// 修改求职人员的面试企业及时间等
	@PostMapping("updateManpower.do")
	public Object updatePeople(@RequestBody JSONObject json)throws Exception
	{
		return staffService.updatePeople(json);
	}
	
	// 状态改变
	@PostMapping("stateChange.do")
	public Object updateChange(@RequestBody JSONObject json)
	{
		return staffService.updateChange(json);
	}
	
	
	
}
