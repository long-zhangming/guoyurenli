package com.example.guoyurenli.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.guoyurenli.service.FileUploadService;

// 接收文件上传的Controller
@Controller
public class FileUploadController
{
	@Resource
	FileUploadService fileUploadService;
	
	// 企业的招商简介文件上传
	@PostMapping("/fileUpload")
	public Object upload(HttpServletRequest request) throws Exception
	{
		return fileUploadService.upload(request);
	}
	
	// 下载招聘简章
	@GetMapping("/u/file")
	public void getCompanyFile(HttpServletRequest request
			, HttpServletResponse response
			, String realName
			, String guid) throws Exception
	{	 
		fileUploadService.getCompanyFile(request, response, realName, guid);
	}
	
	// 厂区内容中插入的图片上传
	@PostMapping("/imgFileUpload")
	public void imgFileUpload(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		JSONObject json = (JSONObject)fileUploadService.imgFileUpload(request);
		
		// 设置应答
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(JSON.toJSONString(json, SerializerFeature.PrettyFormat));
	}
	
}
