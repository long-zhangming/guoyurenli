package com.example.guoyurenli.controller;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.guoyurenli.entity.GyUsers;
import com.example.guoyurenli.service.UserService;

@Controller
public class UserController
{
	@Resource
	private UserService userService;
	
	// 登录页面
	@GetMapping("/login")
	public String login()
	{
		return "login";
	}
	
	// 登录REST
	@PostMapping("/login.do")
	public String logindo(Model model, String userName, String userPwd)
	{
		try
		{
			// 登录校验
			userService.checkLogin(userName, userPwd);
			
			// 取得当前用户的信息放入会话
			Subject subject = SecurityUtils.getSubject();
			GyUsers user = (GyUsers) subject.getPrincipal();
			model.addAttribute("user", user);
			
			// 如果是供应商身份，就直接给他返回到岗位主页
			if(subject.hasRole("supplier"))
			{
				return "enterprise/factoryTop";
			}
			
			return "index";
		} catch (Exception e)
		{
			return "login";
		}
	}
	
	// 注册页面
	@GetMapping("/regist")
	public String regist(String name, String password)
	{
		return "regist";
	}
	
	// 注册用户REST
	@PostMapping("regist.do")
	public Object registdo(String userName
			, String userPassword
			, String userPasswordA
			, Integer roles )
	{
		//// 这里还应该检查一下是不是Boos操作。判断身份,用注解的方式检查了
		return userService.registdo(userName, userPassword, userPasswordA, roles);
	}
	
	// 主页面
//	@GetMapping("index")
//	public String index(Model model)
//	{
//		// 取得当前用户的信息
//		GyUsers user = (GyUsers)SecurityUtils.getSubject().getPrincipal();
//		model.addAttribute("user", user);
//		return "index";
//	}
	
	// 修改密码页面
	@GetMapping("alterAsk")
	public String alterAsk()
	{
		return "alterAsk";
	}
	
	// 修改密码Rest
	@PostMapping("alterAsk.do")
	public Object alterAskdo( String userName, String userPwd
			, String userPwdA, String userPwdB)
	{
		return userService.alterAskdo(userName, userPwd, userPwdA, userPwdB);
	}
	
}
