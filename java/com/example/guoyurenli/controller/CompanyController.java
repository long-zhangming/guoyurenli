package com.example.guoyurenli.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.guoyurenli.entity.GyFactory;
import com.example.guoyurenli.service.CompanyService;

// 管理公司的
@Controller
public class CompanyController
{
	
//	@Autowired
	@Resource
	CompanyService companyService;
	
	// 添加招工企业MVC
	@GetMapping("addCompany")
	public String addCompany()
	{
		return "enterprise/factorysave";
	}
	
	// 添加招工企业REST
	@PostMapping("addCompany.do")
	public Object addCompanydo(@RequestBody GyFactory factory) throws Exception
	{
		return companyService.addCompanydo(factory);
	}
	
	// 公司列表
	@GetMapping("/u/companyList/{attribute}")
	public String companyList(
			HttpServletRequest request
			, Model model
			, Integer pageNumber	// 第几页，搜索参数
			, String filter 
			, @PathVariable Integer attribute) throws Exception
	{
		return companyService.companyList(request, model, pageNumber, filter, attribute);
	}
	
	// 公司的详细信息
	@GetMapping("/u/factoryfin")
	public String postList(Model model, Integer enterpriseId) throws Exception
	{
		return companyService.postList(model, enterpriseId);
	}
	
	// 公司列表展示的顶部导航栏
	@GetMapping("/u/factoryTop")
	public String factoryTop() throws Exception
	{
		return "enterprise/factoryTop";
	}

	

}
