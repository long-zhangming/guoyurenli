package com.example.guoyurenli.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.guoyurenli.service.ShareService;

@Controller
public class ShareController
{
	
	@Autowired
	ShareService shareService;
	
	// 分享接口，生成信息分享码
	@GetMapping("/staff/share")
	public void share(Model model
			, HttpServletRequest request
			, HttpServletResponse response) throws Exception	// 相册ID
	{
		shareService.share(model, request, response);
	}
	
	// 使用分享码
	@GetMapping("/join")
	public Object join(Model model
			, HttpServletRequest request
			, String code) throws Exception
	{	
		return shareService.join(model, request, code);
	}
}
