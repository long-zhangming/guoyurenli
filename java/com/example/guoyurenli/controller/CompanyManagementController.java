package com.example.guoyurenli.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.alibaba.fastjson.JSONObject;
import com.example.guoyurenli.service.CompanyManagementService;

// 企业管理的类
@Controller
public class CompanyManagementController
{
	@Resource
	CompanyManagementService CMService;
	
	// 公司管理，后台管理页面(页面)
	@GetMapping("/factory/management")
	public String factorymanagement() throws Exception
	{
		return "enterprise/factoryManagement";
	}
	
	// 公司管理，后台管理页面（数据）
	@GetMapping("/factory/management.do")
	public Object factoryManagementdo(
			HttpServletResponse response
			, Integer page		// 第几页
			, Integer limit		// 每页显示条数
			, String name		// 搜索数据
			) throws Exception
	{
		return CMService.factoryManagementdo(response, page, limit, name);
	}
	
	// 企业的置顶、价格、时间、备注的修改
	@PostMapping("/modify/company.do")
	public Object modifyCompanydo(@RequestBody JSONObject json) throws Exception
	{
		return CMService.modifyCompanydo(json);
	}
	
	// 删除一个公司
	@PostMapping("/factory/delect.do")
	public Object delectFactory(@RequestBody JSONObject json) throws Exception
	{
		return CMService.delectFactory(json);
	}
	
	
}
